import React, { Component } from 'react';
import { CSVLink } from 'react-csv';
import bibtexReader from '../data_processing/bibtexReader';
import exportToTxt from '../export/exportToTxt';
import bibtexStringifyScopus from '../data_processing/bibtexStringifyScopus';
import bibtexStringifyAPA from '../data_processing/bibtexStringifyAPA';
import '../ExportScreen/ExportScreen.css';
import styled from 'styled-components';

const Button = styled.button`
  background-color: green; /* Green */
  border: none;
  color: white;
  padding: 15px 32px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 16px;
  -webkit-transition-duration: 0.4s; /* Safari */
  transition-duration: 0.4s;

  &:hover{
    background-color: #4CAF50; /* Green */
    color: white
  }
`;
// Headers for exported csv
const csvHeader = ["PUBLICATIONS - JOURNAL, REVIEW, CONFERENCE, OTHERS"];

class ExportScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      csvData: [],
      xlsxData: [],
    }
  }

  processCsvData = (data) => {
    // Same format as data but separated each authors in every publication
    const formattedData = [];
    
    // Splitting authors
    for (const index in data) {
      const authors = data[index].author.split(' and ');
      for (const authorIndex in authors) {
        formattedData.push({
          ...data[index],
          author: authors[authorIndex]
        });
      }
    }

    // Output data
    const csvData = [];
    // Headings
    csvData.push([""]); // Empty row between headers and data
    csvData.push([
      "NO.",
      "Monash Author",
      "School",
      "Monash Author",
      "School",
      "CO AUTHOR",
      "DOCUMENT TITLE",
      "SOURCE TITLE",
      "VOLUME",
      "ISSUE",
      "PAGE START",
      "PAGE END",
      "YEAR",
      "DOI",
      "School",
      "Reference / Link",
      "IND. (Y/N)",
      "INT. (Y/N)",
      "NAT. (Y/N)",
      "Rank",
      "Affiliation",
      "Authors w Affiliations",
      "Remarks",
      "Online Form",
    ]);

    formattedData.forEach((obj) => {
      csvData.push([
        "",               // no.
        obj.author,       // monash author
        obj.affiliation,  // school
        "",               // monash author
        "",               // school
        "",               // co author
        obj.title,        // document title
        obj.journal,      // source title
        obj.volume,       // volume
        "",               // issue
        obj.pages.split("-")[0],  // page start
        obj.pages.split("-")[1],  // page end
        obj.year,         // year
        obj.doi,          // doi
        "",               // school (TODO)
        obj.url,          // reference
        "N",              // industry
        "N",              // international
        "N",              // national
        "",               // rank
        obj.affiliation,  // affliation
        "",               // authors w affiliation (TODO)
      ])
    });

    return csvData;
  }
  


  render() {

    // Process raw data
    const txt = require("../mockBibtex.txt");
    const [allText, data] = bibtexReader(txt);
    return (
        <div className="Export" >
        <h1>Export Page</h1>
        <div>
        <center>
          <CSVLink
              filename="Publications.csv"
              data={this.processCsvData(data)}
              headers={csvHeader}
            >
            <Button>
              Download as CSV
            </Button>
            </CSVLink>

            <Button  onClick={() => exportToTxt(bibtexStringifyScopus(data), "Scopus")}>
                Download as txt (Scopus)
            </Button>

            <Button  onClick={() => exportToTxt(bibtexStringifyAPA(data), "APA")}>
                Download as txt (APA)
            </Button>

            <Button  onClick={() => exportToTxt(allText, "Bibtex")}>
                Download as bibtex
            </Button>
        </center>
          
        </div>
        

      </div>
      
    );
  }
}

export default ExportScreen;
