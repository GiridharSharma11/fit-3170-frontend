// Convert bibtex object to APA formatted string
const bibtexStringifyAPA = bibObject => {
  var txtString = "";
  bibObject.forEach(article => {
    txtString += article.author + "(" + article.year + "). " + article.title + ". " +
    article.journal + ", " + article.volume + ", " +  article.pages + 
    ". doi:" + article.doi + "\n\n";
  })
  return txtString;
}

export default bibtexStringifyAPA;
