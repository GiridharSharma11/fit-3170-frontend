// Reads text files
// Reference: https://gist.github.com/longsangstan/3c259ed76c25784bd17694aab05a37af
const bibtexReader = file => {
    var bibTexObject = [];
    var returnObject = [];
    var allText = "";
    var rawFile = new XMLHttpRequest();
    var keyList = [];
    var dataSet = [];

    rawFile.open("GET", file, false);
    rawFile.onreadystatechange = () => {
        if (rawFile.readyState === 4) {
            if (rawFile.status === 200 || rawFile.status === 0) {
                allText = rawFile.responseText + "\n";
            }
        }
    };
    rawFile.send(null);
    
    var articles = allText.split("@ARTICLE").splice(1, allText.length - 1);// Splits each article citation into respective fields

    articles.forEach((article, index, array) => {
      array[index] = article.split("\n").splice(1, article.split("\n").length - 4)
    })
  
    // Splits each field to a key, value pair array
    articles.forEach(article => {
      article.forEach((field, index, array) => {
        if (array[index].split("=")[0] === 'url') {
          var splitIndex = array[index].indexOf('=');
          array[index] = [array[index].slice(0, splitIndex), array[index].slice(splitIndex+1)]
        } else {
          array[index] = field.split("=")
        }
        // Remove curly braces around the value of the key-value pair
        array[index][1] = array[index][1].slice(1, array[index][1].length-3)
      })
    })
  
    // Convert the array into an array of object for plugging into the library
    articles.forEach(article => {
      var newObject = {};
      article.forEach(field => {
        newObject[field[0]] = field[1]
      })
      bibTexObject.push(newObject);
    })

    // Populate object with empty "" values for non-existing keys
    bibTexObject.forEach(object => {
      var keys = Object.keys(object);
      keyList = keyList.concat(keys);
      keyList = [...new Set(keyList)]
    })

    bibTexObject.forEach(object => {
      var data = [];
      keyList.forEach(key => {
        data.push(object[key]);
      })
      dataSet.push(data);
    })
    
    for (var i=0; i < dataSet.length; i++) {
      var articleObject = {};
      for (var j=0; j < keyList.length; j++) {
        articleObject[keyList[j]] = dataSet[i][j] || "";
      }
      returnObject.push(articleObject);
    }

    return [allText, returnObject];
}

export default bibtexReader;
