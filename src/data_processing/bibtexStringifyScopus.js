// Convert bibtex object to Scopus formatted string
const bibtexStringifyScopus = bibObject => {
  var txtString = "";
  bibObject.forEach(article => {
    txtString += article.author + "\n" + article.title + "\n(" + article.year + ") " +
    article.journal + ", " + article.volume + ", pp. " + article.pages + "\n" +
    article.url + "\n\nDOCUMENT TYPE: " + article.document_type + "\nPUBLICATION STAGE: " +
    "\nSOURCE: " + article.source + "\n\n" 
  })
  return txtString;
}

export default bibtexStringifyScopus;
