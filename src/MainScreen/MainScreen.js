import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import logo from './logo.svg';
import './MainScreen.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <p>Publication Record System</p>
          <img src={logo} className="App-logo" alt="logo" />
        </header>
        {/* Temp working page for export module */}
        <p>
          <Link to="/export">Export Module(Temp)</Link>
        </p>
        {/* Team H20 work here */}
        <p>Visualization do here....</p>
      </div>
    );
  }
}

export default App;
