// Export input string into txt file
// Reference: https://stackoverflow.com/questions/43135852/javascript-export-to-text-file
const exportToTxt = (txtString, type) => {
  const element = document.createElement("a");
  const file = new Blob([txtString], {type: 'text/plain'});
  element.href = URL.createObjectURL(file);
  element.download = `${type}TxtExport.txt`;
  document.body.appendChild(element); // Required for this to work in FireFox
  element.click();
}

export default exportToTxt;
