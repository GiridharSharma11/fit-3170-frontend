import React, { Component } from 'react';
import { BrowserRouter, Route } from 'react-router-dom';
import MainScreen from './MainScreen/MainScreen';
import ExportScreen from './ExportScreen/ExportScreen';

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <div>
          <Route path="/" component={MainScreen} exact />
          <Route path="/export" component={ExportScreen} />
        </div>
      </BrowserRouter>
    );
  }
}

export default App;
